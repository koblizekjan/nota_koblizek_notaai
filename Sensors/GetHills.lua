local sensorInfo = {
	name = "GetHills",
	desc = "Gets locations of hills differentiating between guarded and unguarded",
	author = "Jan Koblížek",
	date = "2021-07-24",
	license = "MIT",
}


searchStep = 100
maxHillSize = 200

function getInfo()
	return {
		period = -1
	}
end


function allHills(minHeight)
    local hills = {}
    local mapWidth = Game.mapSizeX
    local mapHeight = Game.mapSizeZ

    for x = 0, mapWidth, searchStep do
        for z = 0, mapHeight, searchStep do
            local y = Spring.GetGroundHeight(x,z)
            if y > minHeight then
                local pos = Vec3(x,y,z)
				if isNew(pos, hills) then
					local newPos = improvePos(pos, minHeight)
					table.insert(hills, newPos)
				end
            end
        end
    end
    return hills
end

function isNew(pos,hills)
    for _, hill in ipairs(hills) do
        if (hill - pos):Length() < 2*maxHillSize then 
            return false
        end
    end
    return true
end

function improvePos(pos, minHeight)
	local xMin, xMax, zMin, zMax = math.huge, -math.huge, math.huge, -math.huge
    for x = pos.x-maxHillSize,pos.x+maxHillSize,searchStep/5 do
        local y = Spring.GetGroundHeight(x,pos.z)
        if y > minHeight then
            if x < xMin then xMin = x end
            if x > xMax then xMax = x end
        end
    end

    for z = pos.z-maxHillSize,pos.z+maxHillSize,searchStep/5 do
        local y = Spring.GetGroundHeight(pos.x,z)
        if y > minHeight then
            if z < zMin then zMin = z end
            if z > zMax then zMax = z end
        end
    end

    local x = (xMin + xMax) / 2
	local z = (zMin + zMax) / 2
    return Vec3(x, Spring.GetGroundHeight(x,z), z)
end


function isGuarded(hill,enemyPositions)
    for _, pos in ipairs(enemyPositions) do
        if (hill - pos):Length() < maxHillSize then 
            return true
        end
    end
    return false
end


return function(minHeight, enemyPositions)
    local hills = allHills(minHeight)
    local guarded = {}
    local unguarded = {}

    for _, hill in ipairs(hills) do
        if isGuarded(hill, enemyPositions) then 
            table.insert(guarded, hill)
        else
            table.insert(unguarded, hill)
        end
    end
    return {
        guarded = guarded,
        unguarded = unguarded
    }
end